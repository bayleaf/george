( Example 1.  A set of 20 numbers is to be read in,
  and their mean and root mean square computed and puched out.
  A complete program follows:-
)

1, 20 R| (a)                                       (read in data)
0, 1, 20 rep (j) j | a + ] 20 % (P) ;              (calculate and punch mean)
0, 1, 20 rep (j) j | a dup * + ] 20 % .5 pow (P) ; (calculate and punch root mean sq.)
