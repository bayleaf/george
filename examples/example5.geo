( Example 5.  To iterate from an initial value unity using the formula
  x[n + 1] = 1/2(x[n] + y/x[n]) until successive values differ
  by not more than e, write
)

2 (y) ; 1e-5 (e) ; (calculate sqrt 2)

1 :0 (x) y x % + 2 % dup x - mod e > 0^

(the result is left in the accumulator)

(P) ;
]