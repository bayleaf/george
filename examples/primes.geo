(Punch the primes < 1000)

1 1000 rep (p)
    p 20! 0 = 10^ (jump if not prime)
    p (P);
:10
    ]
]

(Subroutine Is TOS a prime?  Uses vars n and l)	
:20
	dup 1 > ~ 27^ (jump if <= 1, not prime)
	dup 2 = 25^ (jump if prime)
	dup 3 = 25^ (jump if prime)
	        
    (divisors 2, 3, 6n +/- 1)
    dup 2 rem 0 = 27^ (jump if not prime)
    dup 3 rem 0 = 27^ (jump if not prime)
    
    (for n := 6 step 6 until sqr(p) + 1)
    0 (n);
    dup sqr 1 + (l) ; (limit)
    22^
:21
    dup n 1 - rem 0 = 27^ (jump if not prime)
    dup n 1 + rem 0 = 27^ (jump if not prime)
:22
    n 6 + (n) l > ~ 21^

:25
    ; 1 29^ (prime)
:27
	; 0
:29
]