( Example 2.  To repeat the above program n times
  for different sets of data, where n is given on the first card,
  prefix with "R (n) ; 1 n rep (x)" or "1 R rep (x) and
  add "]" at end. 
)
1 R rep (x)
1, 20 R| (a)                                       (read in data)
0, 1, 20 rep (j) j | a + ] 20 % (P) ;              (calculate and punch mean)
0, 1, 20 rep (j) j | a dup * + ] 20 % .5 pow (P) ; (calculate and punch root mean sq.)
]
