(Regression lines and correlation coefficient.
 data n, x1, y1, x2, y2 ... xn, yn.
 Algorithm and data from "Electronic Computers,
 S.H. Hollingdale and G.C. Tootill p216..222)
 
 0 (a) (Sigma x)
   (b) (Sigma x^2)
   (c) (Sigma y)
   (d) (Sigma y^2)
   (e) (Sigma x*y)
   ;

 1 R (n) rep (i)
     R
     dup a + (a);
     dup dup * b + (b);
     R
     dup c + (c);
     dup dup * d + (d);
     * e + (e);
]

(n * Sigma x*y - Sigma x * Sigma y)
n e * a c * - dup

( / n * Sigma x^2 - (Sigma x)^2)
n b * a a * - % (f);   (slope regression line y on x)

( / n * Sigma y^2 - (Sigma y)^2)
n d * c c * - % (g);   (slope regression line x on y)

c f (P) a * - n % (P); (Intercept regression line y on x)
a g (P) c * - n % (P); (Intercept regression line x on y)

(Correlation coefficient)
f g * sqr
f 0 > 10^
neg
:10
(P);

]
