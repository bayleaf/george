( Example 3. To calculate the definite integral
  S 1 5 exp(-1/2 * x^2) / (1 + exp(-a*x)) dx
  by Simpson's rule with 16 intervals of argument, for each of
  five values of a, as given on cards.
  Let s0, s1, ... s16 represent a "Simpson vector",
  i.e with the values 1/48, 4/48, 2/48, 4/48, ... 1/48 : this
  can be read in from cards with "0, 16 R|(s)" or it can be
  prepared by a preliminary section of program, e.g.:
 )
 
 1, 48 % 0 | (s) 16 | (s) 2 * 1, 7 rep (j) j 2 * | (s) ]
 2 * 0, 7 rep (j) j 2 * 1 + | (s) ] ;
 
 ( Now the main part of the program can be set out as
   follows:
 )
 
 1, 5 R| (a)              (read in values a1, ... a5)
 1, 5 rep (i) i | a (a) ; (repeat as follows with a = a1, ... a5 in turn)
 0, 0 16 rep (j)          (mark acc. space with zero and repeat as follows for j from 0 to 16)
 j 4 % 1 + (x)            (calculate argument)
 dup * 2 % neg exp x a * neg exp 1 + % (calculate integrand)
 j | s * + ]              (multiply by Simpson coefficient and add; end inner repeat)
 i | (r) ; ]              (store result; end outer repeat)
 1, 5 P| (r)              (punch results)
 
  