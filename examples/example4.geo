( Example 4.  To read in two numbers and punch the square root of the
  sum of the squares, using subroutine no. 6 as above: the
  example program is:
)

R R 6! (P) ;                  (main program)
]                             (bracket to indicate end of main program)

:6 dup * rev dup * + .5 pow ] (subroutine)

