(Linear insertion sort. Data: count of numbers n, a[0], ... a[n - 1], n <= 1024)

0 R (n) 1 - R| (a)   (read the count and the numbers to sort)

 (f marks the first unsorted item in a[].
   Items in positions 0 to f-1 are kept sorted)

1 n 1 - rep (f)
    (a[0] to a[f-1] are sorted. Save the unsorted item into a temporary)
    f | a (t) ;
    (start with the item immediately "left" of f)
    f 1 - (l) ;
    (shifting items that are > t)
   
:10
	l 0 > l 0 = \/ ~ 20^
	l | a t > ~ 20^
	
	(shift it "right")
	l | a l 1 + | (a) ;
	l 1 - (l) ;
	10^
:20
    
    (either l ==-1 || a[l] <= t so t goes in the position to its "right")
    t l 1 + | (a) ;
    (a[0] to a[f] are now sorted)
]

(punch sorted array)
0 n 1 - P| (a)
