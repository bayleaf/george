An implementation of the GEORGE programmimg language\(1\)
=======================================================

Invented by Charles Leonard Hamblin in 1957 for the DEUCE computer. The
first ever RPN programming language.

​\(c\) Copyright Paul Overell 2020-04-16.

I have used the GEORGE PROGRAMMING AND OPERATION MANUAL\(3\) as a
language specification and implemented it in C++ without reference to
the DEUCE source code.

Language text
-------------

The original GEORGE used hand written programs that were then manually
tokenized into pairs of decimal numbers punched on cards. This meant
that characters not available to the DEUCE could be used in the written
programs, e.g. Greek letters. For this implementation us-asii characters
are substituted as follows:

| GEORGE   | us-ascii | comment |
| -------- | -------- | ------- |
| ,        | ,        |         |
| ;        | ;        |         |
| \*       | :        | \* is used for multiply |
| wait     | wait     |         |
| +        | +        |         |
| -        | -        |         |
| ×        | \*       | multiplication sign |
| ÷        | %        | division sign |
| neg      | neg      |         |
| mod      | mod      |         |
| max      | max      |         |
| dup      | dup      |         |
| rev      | rev      |         |
| =        | =        |         |
| \>       | \>       |         |
| /        | /        |         |
| //       | //       |         |
| ~        | ~        |         |
| &        | &        |         |
| ∨        | \\/      | logical or |
| \]       | \]       |         |
| ↓        | \!       | downwards arrow |
| ↑        | ^        | upwards arrow |
| rep      | rep      |         |
| I        | I        |         |
| 0..9     | 0..9     |         |
| a..n     | a..n     |         |
| θ        | o        | Greek small letter theta |
| p..z     | p..z     |         |
| α        | A        | Greek small letter alpha |
| β        | B        | Greek small letter beta |
| γ        | C        | Greek small letter gamma |
| λ        | D        | Greek small letter lambda |
| μ        | E        | Greek small letter mu |
| ν        | F        | Greek small letter nu |
| \(a\)..\(n\) | \(a\)..\(n\) |         |
| \(θ\)      | \(o\)      | Greek small letter theta |
| \(p\)..\(z\) | \(p\)..\(z\) |         |
| \(α\)      | \(A\)      | Greek small letter alpha |
| \(β\)      | \(B\)      | Greek small letter beta |
| \(γ\)      | \(C\)      | Greek small letter gamma |
| \(λ\)      | \(D\)      | Greek small letter lambda |
| \(μ\)      | \(E\)      | Greek small letter mu |
| \(ν\)      | \(F\)      | Greek small letter nu |
| log      | log      |         |
| exp      | exp      |         |
| pow      | pow      |         |
| rem      | rem      |         |
| √        | sqr      | square root |
| sin      | sin      |         |
| cos      | cos      |         |
| R        | R        |         |
| \(P\)    | \(P\)    |         |
| R/       | R/       |         |
| P/       | P/       |         |
| R//      | R//      |         |
| P//      | P//      |         |

Note: Markdown format seems to be unable to represent a |
in a table, so here a / is shown instead, but should be a |
in GEORGE source files.

White space
-----------

Tokens may not contain white space. Tokens may be separated by white
space. Tokens must be separated by white space if otherwise ambiguous.
E.g. "sin" is one token, "s i n" is three tokens; "R||" is one token, "R
||" is two tokens.

Comments
--------

As GEORGE was tokenized manually there was no comment convention.
However, in the examples in the manual parenthesised comments are used,
so this has been adpted for this implementation. Note that comments can
nest. There is an ambiguity in that store and punch instructions are
notated \(a\) and \(P\), in these case the instructions always take
precedence. Therefore \(a\) is an instruction, \(aa\) is a comment.

Floating point constants
------------------------

GEORGE did not have a notation for the exponent part of a floating point
constant in the program text, although exponents could be specified for
numbers on data cards. In this implmentation C++ syntax for non-negative
constants may be used, e.g. 23.78e-39

Restrictions
------------

Jumping into or out of subroutines or rep loops is not supported. This
is not policed. Doing so will corrupt the return stack.

Build
-----

The implementation is for Linux using cmake tested on openSUSE 15.1, but
I expect any modern Linux or BSD distribution cound be used. In a
suitable workspace directory:

    git clone https://bayleaf@bitbucket.org/bayleaf/george.git
    mkdir build
    cd build
    cmake ../george
    make
    make test

The executable will be in bin

Usage
-----

    george source-file reader-file

If the source file or reader file is absent then stdin is used. The
punch output is stdout. Compiler and runtime errors are sent to stderr.

References
----------

1.  [GEORGE programmimg
    language](https://en.wikipedia.org/wiki/GEORGE_(programming_language))
2.  [DEUCE
    Documents](http://members.iinet.net.au/~dgreen/deuce/deucdocs.html)
3.  [GEORGE Programming
    Manual](http://members.iinet.net.au/~dgreen/deuce/GEORGEProgrammingManual.pdf)

