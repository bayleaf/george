#ifndef _SRC_POS_H_
#define _SRC_POS_H_

#include <string>
#include <iostream>

namespace george
{

/**
 * Position in source
 */
class SrcPos
{
public:
    /**
     * @param line
     * @param col
     */
    SrcPos(int line = 0, int col = 0);

    /**
     * Set the column
     * 
     * @param[in] column
     */
    void SetCol(int col);

    /**
     * @return the column
     */
    int GetCol() const;

    /**
     * Set the line
     * 
     * @param[in] line
     */
    void SetLine(int line);

    /**
     * @return the line
     */
    int GetLine() const;

    /**
     * String representation on the position in the source
     */
    std::string ToString() const;

private:
    int m_line = 0;
    int m_col = 0;

};

/**
 * Output source position as text
 * @param[in] os output stream
 * param[in] p position
 */
std::ostream& operator<<(std::ostream& os, const SrcPos& position);

} // namespace george

#endif // _SRC_POS_H_
