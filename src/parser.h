#ifndef _PARSER_H_
#define _PARSER_H_

#include "lexer.h"
#include <cstdint>
#include <map>
#include <vector>

namespace george
{

/**
 * Parser
 */
class Parser
{
public:
    Parser(Lexer& lex);
    
    /**
     * Parse the program
     */
    void Program();
    
    /**
     * Get the compiled program
     * 
     * @return program
     */
    const std::vector<uint8_t>& GetProgram() const
    {
        return m_program;
    }
    
    /**
     * Get the "card reader"
     * 
     * @return card reader
     */
    const std::vector<double>& GetReader() const
    {
        return m_reader;
    }

    /**
     * Get map from label to ip
     * 
     * @return map
     */
    const std::map<double, int>& GetLabelToIp() const
    {
        return m_labelToIp;
    }

private:
    void Section();
    void Subroutine();
    
    Lexer& m_lex;
    std::vector<double> m_reader;
    std::vector<uint8_t> m_program;
    std::map<double, int> m_labelToIp;
};

} // namespace george

#endif // _PARSER_H_