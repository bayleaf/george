#include "lexer.h"
#include <cctype>
#include <algorithm>
#include <stdexcept>

namespace george
{

Lexer::Lexer(std::istream& src)
        : m_src(src)
{
    if (!src.good())
        throw std::runtime_error("Could not open file");
    
    // map hardware representation to token
    for (const HardwareRep hwr : hardwareRep)
    {
        for (const char * const rep : hwr.reps)
        {
            std::string r { rep };
            m_hwrToTok[r] = hwr.token;
        }
    }
    NextChar();
    NextToken();
}

Token Lexer::GetCurToken() const
{
    return m_curToken;
}

void Lexer::NextChar()
{
    m_src.get(m_curChar);
    if (m_src.eof())
    {
        m_curChar = 0;
    }
    else if (m_curChar == '\n')
    {
        m_curPosition.SetLine(m_curPosition.GetLine() + 1);
        m_curPosition.SetCol(0);
    }
    else
    {
        m_curPosition.SetCol(m_curPosition.GetCol() + 1);
    }
}

void Lexer::SkipWhitespace()
{
    while (((m_curChar >= 0 && m_curChar <= ' ') || m_curChar == ',') && !m_src.eof())
    {
        NextChar();
    }
}

void Lexer::NextToken()
{
    for (;;)
    {
        SkipWhitespace();
        m_position = m_curPosition; // position of token
        m_curTokenText.clear();
        
        if (m_src.eof())
        {
            m_curToken = TokEof;
            m_curTokenText = "end of file";
            return;
        }
        
        if (m_curChar == '(')
        {
            // comment, name or punch
            // (text...(more text)..), (a) or (P)
            m_curTokenText = m_curChar;
            NextChar();
            const char c = m_curChar;
            if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'F') || c == 'P')
            {
                m_curTokenText += m_curChar;
                NextChar();
                if (m_curChar == ')')
                {
                    m_curTokenText += m_curChar;
                    m_curToken = (c == 'P') ? TokPunch : TokName;
                    NextChar();
                    return;
                }
            }
            
            // comment.  Note comments nest (this (is) a (comment))
            int countParen{1};
            while (countParen > 0)
            {
                if (m_src.eof())
                    break;
                
                if (m_curChar == '(')
                    ++countParen;
                else if (m_curChar == ')')
                    --countParen;
          
                NextChar();
            }
        }
        else
        {
            break;
        }
    }

    if (::isalpha (m_curChar))
    {
        // identifier or reserved word
        m_curToken = TokUnknown;
        m_curTokenText = m_curChar;
        NextChar();
        while (::isalpha(m_curChar) && !m_src.eof())
        {
            m_curTokenText += m_curChar;
            NextChar();
        }

        // reserved word?
        auto it = m_hwrToTok.find(m_curTokenText);
        if (it != m_hwrToTok.end())
        {
            m_curToken = it->second;
            
            if (m_curToken == TokReader && m_curChar == '|')
            {
                NextChar();
                if (m_curChar == '|')
                {
                    m_curToken = TokReaderMat;
                    NextChar();
                }
                else
                {
                    m_curToken = TokReaderVec;
                }
            }
            else if (m_curToken == TokPunch && m_curChar == '|')
            {
                NextChar();
                if (m_curChar == '|')
                {
                    m_curToken = TokPunchMat;
                    NextChar();
                }
                else
                {
                    m_curToken = TokPunchVec;
                }
            }
        }
        else
        {
            // identifiers are a..z A..F
            if (m_curTokenText.size() == 1)
            {
                const char c = m_curTokenText[0];
                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'F'))
                {
                    m_curToken = TokIdentifier;
                }
            }
        }
    }
    else if (::isdigit(m_curChar))
    {
        // number
        m_curTokenText = m_curChar;
        NextChar();
        m_curToken = TokRealLiteral;

        while (::isdigit(m_curChar) && !m_src.eof())
        {
            m_curTokenText += m_curChar;
            NextChar();
        }
        if (m_curChar == '.')
        {
            DecimalFraction();
        }
        else if (m_curChar == 'e')
        {
            ExponentPart();
        }
        else
        {
            m_curToken = TokIntegerLiteral;
        }
    }
    else if (m_curChar == '.')
    {
        // decimal point
        m_curToken = TokRealLiteral;
        m_curTokenText = "0";
        DecimalFraction();
        if (m_curChar == 'e')
        {
            ExponentPart();
        }
    }
    else
    {
        // try for digraph
        m_curTokenText = m_curChar;
        NextChar();
        m_curTokenText += m_curChar;

        auto it = m_hwrToTok.find(m_curTokenText);
        if (it != m_hwrToTok.end())
        {
            m_curToken = it->second;
            NextChar();
            return;
        }

        // try for unigraph
        m_curTokenText.pop_back();
        it = m_hwrToTok.find(m_curTokenText);
        if (it != m_hwrToTok.end())
        {
            m_curToken = it->second;
            return;
        }

        m_curToken = TokUnknown;
    }
}

void Lexer::DecimalFraction()
{
    m_curTokenText += ".";
    NextChar();
    while (::isdigit(m_curChar) && !m_src.eof())
    {
        m_curTokenText += m_curChar;
        NextChar();
    }

    if (m_curChar == 'e')
    {
        ExponentPart();
    }
}

void Lexer::ExponentPart()
{
    // Extension to GEORGE allow the exponent to be specified using C++ syntax "e"
    m_curTokenText += "e"; // C++ syntax
    NextChar();
    if (m_curChar == '+' || m_curChar == '-')
    {
        m_curTokenText += m_curChar;
        NextChar();
    }

    if (!::isdigit(m_curChar))
    {
        Error() << "Bad exponent part" << std::endl;
    }
    while (::isdigit(m_curChar) && !m_src.eof())
    {
        m_curTokenText += m_curChar;
        NextChar();
    }
}

bool Lexer::Have(Token t)
{
    if (m_curToken == t)
    {
        NextToken();
        m_recovering = false;
        return true;
    }
    else
    {
        return false;
    }
}

bool Lexer::HaveIntegerLiteral(int& value)
{
    if (m_curToken == TokIntegerLiteral)
    {
        try
        {
            value = stoi(m_curTokenText);
        }
        catch (std::exception e)
        {
            Error() << e.what() << std::endl;
        }

        NextToken();
        m_recovering = false;
        return true;
    }
    else
    {
        return false;
    }
}

bool Lexer::HaveRealLiteral(double& value)
{
    if (m_curToken == TokRealLiteral)
    {
        try
        {
            value = stod(m_curTokenText);
        }
        catch (std::exception e)
        {
            Error() << e.what() << std::endl;
        }

        NextToken();
        m_recovering = false;
        return true;
    }
    else
    {
        return false;
    }
}

bool Lexer::HaveIdentifier(uint8_t& value)
{
    if (m_curToken == TokIdentifier)
    {
        const char c = m_curTokenText[0];
        value = (c >= 'a' && c <= 'z')
                ? c - 'a'
                : c - 'A' + 26;
        NextToken();
        m_recovering = false;
        return true;
    }
    else
    {
        return false;
    }
}

bool Lexer::HaveName(uint8_t& value)
{
    if (m_curToken == TokName)
    {
        const char c = m_curTokenText[1];
        value = (c >= 'a' && c <= 'z')
                ? c - 'a'
                : c - 'A' + 26;
        NextToken();
        m_recovering = false;
        return true;
    }
    else
    {
        return false;
    }
}

void Lexer::MustBe(Token t)
{
    if (!m_recovering)
    {
        if (Have(t))
            return;

        m_recovering = true;
        Error() << "\"" << m_curTokenText << "\" found where \"" << t << "\" expected" << std::endl;
    }
    else
    {
        while (!Have(t) && !Have(TokEof))
            NextToken();
    }
}

void Lexer::MustBeIntegerLiteral(int& value)
{
    if (!m_recovering)
    {
        if (HaveIntegerLiteral(value))
            return;

        m_recovering = true;
        Error() << "\"" << m_curTokenText << "\" found where \"integer literal\" expected" << std::endl;
    }
    else
    {
        while (!HaveIntegerLiteral(value) && !Have(TokEof))
            NextToken();
    }
}

void Lexer::MustBeRealLiteral(double& value)
{
    if (!m_recovering)
    {
        if (HaveRealLiteral(value))
            return;

        m_recovering = true;
        Error() << "\"" << m_curTokenText << "\" found where \"real literal\" expected" << std::endl;
    }
    else
    {
        while (!HaveRealLiteral(value) && !Have(TokEof))
            NextToken();
    }
}

void Lexer::MustBeIdentifier(uint8_t& identifier)
{
    if (!m_recovering)
    {
        if (HaveIdentifier(identifier))
            return;

        m_recovering = true;
        Error() << "\"" << m_curTokenText << "\" found where \"identifier\" expected" << std::endl;
    }
    else
    {
        while (!HaveIdentifier(identifier) && !Have(TokEof))
            NextToken();
    }
}

void Lexer::MustBeName(uint8_t& name)
{
    if (!m_recovering)
    {
        if (HaveName(name))
            return;

        m_recovering = true;
        Error() << "\"" << m_curTokenText << "\" found where \"name\" expected" << std::endl;
    }
    else
    {
        while (!HaveName(name) && !Have(TokEof))
            NextToken();
    }
}

SrcPos Lexer::GetPosition() const
{
    return m_position;
}

int Lexer::GetErrors() const
{
    return m_errors;
}

std::ostream& Lexer::Error()
{
    std::cerr << "Error: " << m_position << " ";
    ++m_errors;
    return std::cerr;
}

std::ostream& operator<<(std::ostream& os, const Token t)
{
    const char* text{nullptr};
    switch (t)
    {
    case TokUnknown:
        text = "uknown";
        break;
    case TokIdentifier:
        text = "identifier";
        break;
    case TokIntegerLiteral:
        text = "integer literal";
        break;
    case TokRealLiteral:
        text = "real literal";
        break;
    case TokEof:
        text = "end of file";
        break;
    default:
        for (const HardwareRep hwr : hardwareRep)
        {
            if (hwr.token == t)
            {
                text = hwr.reps[0];
                break;
            }
        }
        break;
    }
    os << text;
    return os;
}

} //namespace george
