aux_source_directory(${CMAKE_CURRENT_SOURCE_DIR} SOURCE)
list(REMOVE_ITEM SOURCE main.cpp)
add_library(georgelib STATIC ${SOURCE})

add_executable(george ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp)
target_link_libraries(george georgelib)
