#include "parser.h"
#include "opcodes.h"
#include <algorithm>
#include <map>

namespace george
{

Parser::Parser(Lexer& lex)
        : m_lex(lex)
{

}

void Parser::Program()
{
    // main program
    Section();
    m_program.push_back(OpStop);
    
    // subroutines
    while (m_lex.Have(TokColon))
    {
        Subroutine();
        m_program.push_back(OpRet);
    }
    
    m_lex.MustBe(TokEof);
}

void Parser::Section()
{
    bool conditional{false};
    
    while (m_lex.GetCurToken() != TokKetSquare && !m_lex.Have(TokUnknown) && !m_lex.Have(TokEof))
    {
        switch (m_lex.GetCurToken())
        {
        // george words
        case TokCos:
            m_lex.NextToken();
            m_program.push_back(OpCos);
            break;
            
        case TokDup:
            m_lex.NextToken();
            m_program.push_back(OpDup);
            break;

        case TokExp:
            m_lex.NextToken();
            m_program.push_back(OpExp);
            break;

        case TokLog:
            m_lex.NextToken();
            m_program.push_back(OpLog);
            break;

        case TokMax:
            m_lex.NextToken();
            m_program.push_back(OpMax);
            break;

        case TokMod:
            m_lex.NextToken();
            m_program.push_back(OpMod);
            break;

        case TokNeg:
            m_lex.NextToken();
            m_program.push_back(OpNeg);
            break;

        case TokPow:
            m_lex.NextToken();
            m_program.push_back(OpPow);
            break;

        case TokI:
            m_lex.NextToken();
            m_program.push_back(OpInput);
            break;

        case TokPunch:
            m_lex.NextToken();
            m_program.push_back(OpPunch);
            break;

        case TokPunchVec:
        {
            m_lex.NextToken();
            uint8_t name{0};
            m_lex.MustBeName(name);
            m_program.push_back(OpPunchVec);
            m_program.push_back(name);
            break;
        }
        case TokPunchMat:
        {
            m_lex.NextToken();
            uint8_t name{0};
            m_lex.MustBeName(name);
            m_program.push_back(OpPunchMat);
            m_program.push_back(name);
            break;
        }
        case TokReader:
            m_lex.NextToken();
            m_program.push_back(OpReader);
            break;

        case TokReaderVec:
        {
            m_lex.NextToken();
            uint8_t name{0};
            m_lex.MustBeName(name);
            m_program.push_back(OpReaderVec);
            m_program.push_back(name);

            break;
        }
        case TokReaderMat:
        {
            m_lex.NextToken();
            uint8_t name{0};
            m_lex.MustBeName(name);
            m_program.push_back(OpReaderMat);
            m_program.push_back(name);
            break;
        }
        case TokRev:
            m_lex.NextToken();
            m_program.push_back(OpRev);
            break;

        case TokRem:
            m_lex.NextToken();
            m_program.push_back(OpRem);
            break;

        case TokRep:
        {
            m_lex.NextToken();
            
            uint8_t name{0};
            m_lex.MustBeName(name);
         
            m_program.push_back(OpRep);
            m_program.push_back(name);
            const int patch = m_program.size();
            
            // reserve space for forward jump to test address
            const int reserve{0};
            const uint8_t * bReserve = reinterpret_cast<const uint8_t*>(&reserve);
            const uint8_t * eReserve = bReserve + sizeof(reserve);
            m_program.insert(m_program.end(), bReserve, eReserve);

            // start of loop address
            const int loop = m_program.size();
            
            Section();
            
            // patch forward jump to test
            const int endLoop = m_program.size();
            const uint8_t * bEndLoop = reinterpret_cast<const uint8_t*>(&endLoop);
            const uint8_t * eEndLoop = bEndLoop + sizeof(endLoop);
            std::copy(bEndLoop, eEndLoop, &m_program[patch]);
            
            m_program.push_back(OpRepEnd);
            m_program.push_back(name);
            
            // backwards jump to start of loop address
            const uint8_t * bLoop = reinterpret_cast<const uint8_t*>(&loop);
            const uint8_t * eLoop = bLoop + sizeof(loop);
            m_program.insert(m_program.end(), bLoop, eLoop);
            break;
        }

        case TokSin:
            m_lex.NextToken();
            m_program.push_back(OpSin);
            break;

        case TokSqr:
            m_lex.NextToken();
            m_program.push_back(OpSqr);
            break;

        case TokWait:
            // ignored
            m_lex.NextToken();
            break;
            
        // relational operators
        case TokGt:
            m_lex.NextToken();
            conditional = true;
            m_program.push_back(OpGt);
            break;

        case TokEq:
            m_lex.NextToken();
            conditional = true;
            m_program.push_back(OpEq);
            break;
        
        // arithmetic operators
        case TokPlus:
            m_lex.NextToken();
            m_program.push_back(OpPlus);
            break;

        case TokMinus:
            m_lex.NextToken();
            m_program.push_back(OpMinus);
            break;

        case TokTimes:
            m_lex.NextToken();
            m_program.push_back(OpTimes);
        break;

        case TokDiv:
            m_lex.NextToken();
            m_program.push_back(OpDiv);
            break;
        
        // Boolean operators
        case TokAnd:
            m_lex.NextToken();
            m_program.push_back(OpAnd);
            break;

        case TokOr:
            m_lex.NextToken();
            m_program.push_back(OpOr);
            break;

        case TokNot:
            m_lex.NextToken();
            m_program.push_back(OpNot);
            break;
        
        // subscript operators
        case TokBar:
        {
            m_lex.NextToken();
            
            uint8_t identifier{0};
            if (m_lex.HaveIdentifier(identifier))
            {
                m_program.push_back(OpLoadVec);
                m_program.push_back(identifier);
            }
            else
            {
                uint8_t name{0};
                m_lex.MustBeName(name);
             
                m_program.push_back(OpStoreVec);
                m_program.push_back(name);
            }
            break;
        }

        case TokBarBar:
        {
            m_lex.NextToken();
            
            uint8_t identifier{0};
            if (m_lex.HaveIdentifier(identifier))
            {
                m_program.push_back(OpLoadMat);
                m_program.push_back(identifier);
            }
            else
            {
                uint8_t name{0};
                m_lex.MustBeName(name);
             
                m_program.push_back(OpStoreMat);
                m_program.push_back(name);
            }
            break;
        }
        
        // other delimiters
        case TokColon:
        {
            m_lex.NextToken();
            
            int mark{0};
            m_lex.MustBeIntegerLiteral(mark);
            if (m_labelToIp.find(mark) != m_labelToIp.end())
            {
                m_lex.Error() << "Mark :" << mark << " already exists" << std::endl;
            }
            else if (mark >= 32)
            {
                m_lex.Error() << "Mark :" << mark << " must be < 32" << std::endl;
            }
            else
            {
                m_labelToIp[static_cast<double>(mark)] = m_program.size();
            }
            break;
        }

        case TokSemicolon:
            m_lex.NextToken();
            m_program.push_back(OpDrop);
            break;

        case TokCall:
            m_lex.NextToken();
            
            if (conditional)
            {
                m_program.push_back(OpCallTrue);
                conditional = false;
            }
            else
            {
                m_program.push_back(OpCall);
            }
            break;

        case TokJmp:
            m_lex.NextToken();
            
            if (conditional)
            {
                m_program.push_back(OpJmpTrue);
                conditional = false;
            }
            else
            {
                m_program.push_back(OpJmp);
            }
            break;

        
        // entities
        case TokName:
        {
            uint8_t name{0};
            m_lex.MustBeName(name);
         
            m_program.push_back(OpStore);
            m_program.push_back(name);
            break;
        }

        case TokIdentifier:
        {
            uint8_t identifier{0};
            m_lex.MustBeIdentifier(identifier);
            m_program.push_back(OpLoad);
            m_program.push_back(identifier);
            break;
        }

        case TokIntegerLiteral:
        {
            int value{0};
            m_lex.MustBeIntegerLiteral(value);
            const uint8_t * b = reinterpret_cast<const uint8_t*>(&value);
            const uint8_t * e = b + sizeof(value);
            
            m_program.push_back(OpIntegerLiteral);
            m_program.insert(m_program.end(), b, e);
            break;
        }

        case TokRealLiteral:
        {
             double value{0};
             m_lex.MustBeRealLiteral(value);
             const uint8_t * b = reinterpret_cast<const uint8_t*>(&value);
             const uint8_t * e = b + sizeof(value);

             m_program.push_back(OpRealLiteral);
             m_program.insert(m_program.end(), b, e);
             break;
         }

        default:
            break;
        }
    }
    
    // ] is only required for the main program if subroutines follow.
    if (!m_lex.Have(TokEof))
    {
        m_lex.MustBe(TokKetSquare);
    }
}

void Parser::Subroutine()
{
    int mark{0};
    m_lex.MustBeIntegerLiteral(mark);
    if (m_labelToIp.find(mark) != m_labelToIp.end())
    {
        m_lex.Error() << "Mark " << mark << " already exists" << std::endl;
    }
    else if (mark >= 32)
    {
        m_lex.Error() << "Mark :" << mark << " must be < 32" << std::endl;
    }
    else
    {
        m_labelToIp[static_cast<double>(mark)] = m_program.size();
    }

    Section();
}

} // namespace george
