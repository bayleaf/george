#include "parser.h"
#include "runtime.h"

#include <stdlib.h>
#include <iostream>
#include <stdexcept>
#include <fstream>

/**
 * george compiler
 * 
 * @param[in] argc number of arguments
 * @param[in] argv arguments.
 * @retval 0 => OK
 * @retval 1 => error
 */
int main(int argc, char *argv[]) 
{
    try
    {
        std::fstream srcFile;
        std::fstream readerFile;
        if (argc >= 2)
        {
            srcFile.open(argv[1], std::fstream::in);
            if (!srcFile.is_open())
                throw std::runtime_error(std::string("Can't open ") + argv[1]);
        }
        if (argc >= 3)
        {
            readerFile.open(argv[2], std::fstream::in);
            if (!readerFile.is_open())
                throw std::runtime_error(std::string("Can't open ") + argv[2]);
        }

        std::istream& src = srcFile.is_open() ? srcFile : std::cin;
        std::istream& reader = readerFile.is_open() ? readerFile : std::cin;
        
        george::Lexer lex(src);
        george::Parser parser(lex);
        parser.Program();
        
        if (lex.GetErrors() == 0)
        {
            george::Runtime runtime;
            runtime.Run(parser.GetProgram(), parser.GetLabelToIp(), reader, std::cout);
        }
    }
    catch (const std::exception & e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    
    return 0;
}