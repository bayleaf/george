#ifndef _OPCODES_H_
#define _OPCODES_H_

#include <cstdint>

namespace george
{

enum Opcode: uint8_t
{
    // functions
    OpCos,
    OpExp,
    OpLog,
    OpMax,
    OpMod,
    OpNeg,
    OpPow,
    OpRem,
    OpSin,
    OpSqr,
    
    // I/O
    OpInput,
    OpPunch,
    OpPunchVec,
    OpPunchMat,
    OpReader,
    OpReaderVec,
    OpReaderMat,
    
    // stack
    OpDrop,
    OpDup,
    OpRev,

    // relational operators
    OpGt,
    OpEq,
    
    // arithmetic operators
    OpPlus,
    OpMinus,
    OpTimes,
    OpDiv,
    
    // Boolean operators
    OpAnd,
    OpOr,
    OpNot,

    // control
    OpCall,
    OpCallTrue,
    OpJmp,
    OpJmpTrue,
    OpRep,
    OpRepEnd,
    OpRet,
    OpStop,
    
    // entities
    OpIntegerLiteral,
    OpRealLiteral,
    
    // load/store
    OpLoad,
    OpStore,
    OpLoadVec,
    OpStoreVec,
    OpLoadMat,
    OpStoreMat,
};

} // namespace george

#endif // _OPCODES_H_