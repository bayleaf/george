#include "runtime.h"
#include "opcodes.h"
#include <cmath>
#include <stdexcept>
#include <sstream>
#include <iostream>

namespace george
{

Runtime::Runtime()
{

}

void Runtime::Run(const std::vector<uint8_t>& program, const std::map<double, int> labelToIp, std::istream& reader, std::ostream& punch)
{
    m_ip = 0;
    m_sp = -1;
    m_rsp = -1;
    
    // note the variables m_vars, m_vecs and m_mats will still contain their values from last run.
        
    try
    {
        for (;;)
        {
            switch (program[m_ip++])
            {
            // functions
            case OpCos:
                m_stack.at(m_sp) = cos(m_stack.at(m_sp));
                break;
            case OpExp:
                m_stack.at(m_sp) = exp(m_stack.at(m_sp));
                break;
            case OpLog:
                m_stack.at(m_sp) = log(m_stack.at(m_sp));
                break;
            case OpMax:
                m_stack.at(m_sp - 1) = std::max(m_stack.at(m_sp), m_stack.at(m_sp - 1));
                --m_sp;
                break;
            case OpMod:
                m_stack.at(m_sp) = fabs(m_stack.at(m_sp));
                break;
            case OpNeg:
                m_stack.at(m_sp) = -m_stack.at(m_sp);
                break;
            case OpPow:
                m_stack.at(m_sp - 1) = pow(m_stack.at(m_sp - 1), m_stack.at(m_sp));
                --m_sp;
                break;
            case OpRem:
                m_stack.at(m_sp - 1) = fmod(m_stack.at(m_sp - 1), m_stack.at(m_sp));
                --m_sp;
                break;
            case OpSin:
                m_stack.at(m_sp) = sin(m_stack.at(m_sp));
                break;
            case OpSqr:
                m_stack.at(m_sp) = sqrt(m_stack.at(m_sp));
                break;
    
            // I/O
            case OpInput:
                throw std::runtime_error("\"I\" is not supported");
                break;
            case OpPunch:
                punch << m_stack.at(m_sp) << std::endl;
                break;
            case OpPunchVec:
                for (double row = m_stack.at(m_sp - 1); row <= m_stack.at(m_sp); ++row)
                {
                    punch << GetVar(program[m_ip], row) << std::endl;
                }
                m_sp -= 2;
                ++m_ip;
                break;
            case OpPunchMat:
                for (double row = m_stack.at(m_sp - 3); row <= m_stack.at(m_sp - 2); ++row)
                {
                    for (double col = m_stack.at(m_sp - 1); col <= m_stack.at(m_sp); ++col)
                    {
                        punch << GetVar(program[m_ip], row, col) << std::endl;
                    }
                }
                m_sp -= 4;
                ++m_ip;
                break;
            case OpReader:
                ++m_sp;
                reader >> m_stack.at(m_sp);
                break;
            case OpReaderVec:
                for (double row = m_stack.at(m_sp - 1); row <= m_stack.at(m_sp); ++row)
                {
                    reader >> GetVar(program[m_ip], row);
                }
                m_sp -= 2;
                ++m_ip;
                break;
            case OpReaderMat:
                for (double row = m_stack.at(m_sp - 3); row <= m_stack.at(m_sp - 2); ++row)
                {
                    for (double col = m_stack.at(m_sp - 1); col <= m_stack.at(m_sp); ++col)
                    {
                        reader >> GetVar(program[m_ip], row, col);
                    }
                }
                m_sp -= 4;
                ++m_ip;
                break;
                
            // m_stack
            case OpDrop:
                --m_sp;
                break;
            case OpDup:
                ++m_sp;
                m_stack.at(m_sp) = m_stack.at(m_sp - 1);
                break;
            case OpRev:
                std::swap(m_stack.at(m_sp), m_stack.at(m_sp - 1));
                break;
                
            // relational operators
            case OpGt:
                m_stack.at(m_sp - 1) = (m_stack.at(m_sp - 1) > m_stack.at(m_sp)) ? -1 : 0;
                --m_sp;
                break;
            case OpEq:
                m_stack.at(m_sp - 1) = (m_stack.at(m_sp - 1) == m_stack.at(m_sp)) ? -1 : 0;
                --m_sp;
                break;
                
            // arithmetic operators
            case OpPlus:
                m_stack.at(m_sp - 1) += m_stack.at(m_sp);
                --m_sp;
                break;
            case OpMinus:
                m_stack.at(m_sp - 1) -= m_stack.at(m_sp);
                --m_sp;
                break;
            case OpTimes:
                m_stack.at(m_sp - 1) *= m_stack.at(m_sp);
                --m_sp;
                break;
            case OpDiv:
                m_stack.at(m_sp - 1) /= m_stack.at(m_sp);
                --m_sp;
                break;
                
            // Boolean operators
            case OpAnd:
                m_stack.at(m_sp - 1) = (m_stack.at(m_sp) != 0 && m_stack.at(m_sp - 1) != 0) ? -1 : 0;
                --m_sp;
                break;
            case OpOr:
                m_stack.at(m_sp - 1) = (m_stack.at(m_sp) != 0 || m_stack.at(m_sp - 1) != 0) ? -1 : 0;
                --m_sp;
                break;
            case OpNot:
                m_stack.at(m_sp) = (m_stack.at(m_sp) == 0) ? -1 : 0;
                break;
                
            // control
            case OpCall:
               ++m_rsp;
                m_rstack.at(m_rsp) = static_cast<double>(m_ip);
                m_ip = labelToIp.at(m_stack.at(m_sp));
                --m_sp;
                break;
            case OpCallTrue:
                if (m_stack.at(m_sp - 1) != 0)
                {
                    ++m_rsp;
                    m_rstack.at(m_rsp) = static_cast<double>(m_ip);
                    m_ip = labelToIp.at(m_stack.at(m_sp));
                }
                m_sp -= 2;
                break;
            case OpJmp:
                m_ip = labelToIp.at(m_stack.at(m_sp));
                --m_sp;
                break;
            case OpJmpTrue:
                if (m_stack.at(m_sp - 1) != 0)
                {
                    m_ip = labelToIp.at(m_stack.at(m_sp));
                }
                m_sp -= 2;
                break;
            case OpRep:
                GetVar(program[m_ip]) = m_stack.at(m_sp - 1) - 1; // initial value (pre decrement)
                m_ip = *reinterpret_cast<const int*>(&program[m_ip + 1]); // jump to end of loop increment for first test
                ++m_rsp;
                m_rstack.at(m_rsp) = m_stack.at(m_sp); // step
                m_sp -= 2;
                break;
            case OpRepEnd:
                if (++GetVar(program[m_ip]) <= m_rstack.at(m_rsp))
                {
                    // loop
                    m_ip = *reinterpret_cast<const int*>(&program[m_ip + 1]);
                }
                else
                {
                    --m_rsp;
                    m_ip += 1 + sizeof(int);
                }
                break;
            case OpRet:
                m_ip = static_cast<int>(m_rstack.at(m_rsp));
                --m_rsp;
                break;
            case OpStop:
                return;
                
            // entities
            case OpIntegerLiteral:
                ++m_sp;
                m_stack.at(m_sp) = static_cast<double>(*reinterpret_cast<const int*>(&program[m_ip]));
                m_ip += sizeof(int);
                break;
            case OpRealLiteral:
                ++m_sp;
                m_stack.at(m_sp) = *reinterpret_cast<const double*>(&program[m_ip]);
                m_ip += sizeof(double);
                break;
                
            // load/store
            case OpLoad:
                ++m_sp;
                m_stack.at(m_sp) = GetVar(program[m_ip]);
                ++m_ip;
                break;
            case OpStore:
                GetVar(program[m_ip]) = m_stack.at(m_sp);
                ++m_ip;
                break;
            case OpLoadVec:
                m_stack.at(m_sp) = GetVar(program[m_ip], m_stack.at(m_sp));
                ++m_ip;
                break;
            case OpStoreVec:
                GetVar(program[m_ip], m_stack.at(m_sp)) = m_stack.at(m_sp - 1);
                --m_sp;
                ++m_ip;
                break;
            case OpLoadMat:
                m_stack.at(m_sp - 1) = GetVar(program[m_ip], m_stack.at(m_sp - 1), m_stack.at(m_sp));
                --m_sp;
                ++m_ip;
                break;
            case OpStoreMat:
                GetVar(program[m_ip], m_stack.at(m_sp - 1), m_stack.at(m_sp)) = m_stack.at(m_sp - 2);
                m_sp -= 2;
                ++m_ip;
                break;
    
            default:
            {
                std::ostringstream ss;
                ss << "Unknown opcode: " << static_cast<int>(program.at(m_ip - 1));
                throw std::runtime_error(ss.str());
            }
            
            }
        }
    }
    catch (const std::exception & e)
    {
        // some diagnostics
        std::cerr << "Runtime error" << std::endl
                << "m_ip=" << m_ip << std::endl
                << "m_sp=" << m_sp << std::endl;
        const int stackLimit = std::min(m_sp, static_cast<int>(m_stack.size()) - 1);
        for (int i = 0; i <= stackLimit; ++i)
            std::cerr << m_stack[i] << std::endl;
        
        std::cerr << "m_rsp=" << m_rsp << std::endl;
        const int rstackLimit = std::min(m_rsp, static_cast<int>(m_rstack.size()) - 1);
        for (int i = 0; i <= rstackLimit; ++i)
            std::cerr << m_rstack[i] << std::endl;

        throw;    
    }
}

double& Runtime::GetVar(uint8_t var)
{
    return m_vars.at(static_cast<int>(var));
}

double& Runtime::GetVar(uint8_t var, double row)
{
    const int r = static_cast<int>(row) % 1024;
    return m_vecs.at(static_cast<int>(var) * stride + r);
}

double& Runtime::GetVar(uint8_t var, double row, double col)
{
    const int r = static_cast<int>(row) % 128;
    const int c = static_cast<int>(col) % 32;
    return m_mats.at((static_cast<int>(var) * stride + r) * stride + c);
}

} // namespace george

