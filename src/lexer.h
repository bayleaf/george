#ifndef _LEXER_H_
#define _LEXER_H_

#include "tokens.h"
#include "src_pos.h"

#include <vector>
#include <map>
#include <cstdint>

namespace george
{

class Lexer
{
public:
    /**
     * Lexical analyser
     * 
     * @param src george source
     */
    Lexer(std::istream& src);

    ~Lexer() = default;
    
    /**
     * Read next character
     */
    void NextChar();
    
    /**
     * Skip whitespace
     */
    void SkipWhitespace();
    
    /**
     * Read next token
     */
    void NextToken();
    
    /**
     * Is this the current token?  If so eat it.
     * 
     * @param[in] t token
     * @return true <=> it is
     */
    bool Have(Token t);
    
    /**
     * Is this an integer literal?  If so eat it.
     * 
     * @param[out] true <=> real
     * @param[out] value
     * @return true <=> it is
     */
    bool HaveIntegerLiteral(int& value);
    
    /**
     * Is this a real literal?  If so eat it.
     * 
     * @param[out] true <=> real
     * @param[out] value
     * @return true <=> it is
     */
    bool HaveRealLiteral(double& value);

    /**
     * Is this an identifier?  If so eat it.
     * 
     * @param[out] value
     * @return true <=> it is
     */
    bool HaveIdentifier(uint8_t& value);
    
    /**
     * Is this a name?  If so eat it.
     * 
     * @param[out] value
     * @return true <=> it is
     */
    bool HaveName(uint8_t& value);
    
    /**
     * This must be the current token, eat it.
     * 
     * @param[in] t token
     */
    void MustBe(Token t);

    /**
      * An integer literal must be the current token, eat it.
      * 
      * @param[out] value
      */
    void MustBeIntegerLiteral(int& value);
    
    /**
      * An real literal must be the current token, eat it.
      * 
      * @param[out] value
      */
    void MustBeRealLiteral(double& value);
    
    /**
     * An identifier must be the current token, eat it.
     * 
     * @param[out] identifier
     */
    void MustBeIdentifier(uint8_t& identifier);
    
    /**
     * A name must be the current token, eat it.
     * 
     * @param[out] name
     */
    void MustBeName(uint8_t& name);
    

    /**
     * Output error message
     * 
     * @return output stream
     */
    std::ostream& Error();
    
    /**
     * Get position in the source
     * 
     * @return position in the source
     */
    SrcPos GetPosition() const;
    
    /**
     * Get current token
     * 
     * @return current token
     */
    Token GetCurToken() const;
    
    /**
     * Get count of errors
     * 
     * @return number of errors
     */
    int GetErrors() const;

private:
    void DecimalFraction();
    void ExponentPart();
    
    std::istream& m_src;
    
    // Current stuff
    char m_curChar{0};
    SrcPos m_curPosition{1,0};
    SrcPos m_position{1,0};
    Token m_curToken{TokUnknown};
    std::string m_curTokenText;
    
    // hardware representation
    std::map<std::string, Token> m_hwrToTok;
    
    // error handling
    bool m_recovering{false};
    int m_errors{0};
};

} // namespace george

#endif // _LEXER_H_
