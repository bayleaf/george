#ifndef _RUNTIME_H_
#define _RUNTIME_H_

#include <cstdint>
#include <array>
#include <vector>
#include <ostream>
#include <map>

namespace george
{

/**
 * GEORGE runtime system
 */
class Runtime
{
public:
    Runtime();
    
    /**
     * Run a GEORGE program
     * 
     * @param[in] program
     * @param[in] reader, card reader
     * @param[in] labelToIp, map label to program address
     * @param[in] punch, card punch
     */
    void Run(const std::vector<uint8_t>& program, const std::map<double, int> labelToIp, std::istream& reader, std::ostream& punch);
    
private:
    
    double& GetVar(uint8_t var);
    double& GetVar(uint8_t var, double row);
    double& GetVar(uint8_t var, double row, double col);
    
    // machine registers
    int m_ip{0};
    int m_sp{-1};
    int m_rsp{-1};
    std::array<double, 12> m_stack; // stack size specified in programmers guide
    std::array<double, 6> m_rstack; // stack size specified in programmers guide
    
    // variables store
    static const int stride{32};
    std::array<double, stride> m_vars; // unsuffixed
    std::array<double, stride * stride> m_vecs; // singally suffixed
    std::array<double, 4 * stride * stride> m_mats; // doubly suffixed
};



} // namespace george

#endif // _RUNTIME_H_
