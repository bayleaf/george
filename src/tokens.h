#ifndef _TOKENS_H_
#define _TOKENS_H_

#include <vector>
#include <string>
#include <iostream>

namespace george
{

enum Token
{
    // unknown
    TokUnknown,
    
    // george words
    TokCos,
    TokDup,
    TokExp,
    TokI,
    TokLog,
    TokMax,
    TokMod,
    TokNeg,
    TokPow,
    TokPunch,       // P
    TokPunchVec,    // P|
    TokPunchMat,    // P||
    TokReader,      // R
    TokReaderVec,   // R|
    TokReaderMat,   // R||
    TokRev,
    TokRem,
    TokRep,
    TokSin,
    TokSqr,
    TokWait,

    // relational operators
    TokGt,  // >
    TokEq,  // =
    
    // arithmetic operators
    TokPlus,
    TokMinus,
    TokTimes,
    TokDiv,
    
    // Boolean operators
    TokAnd, // &
    TokOr,  // \/
    TokNot, // ~
    
    // subscript operators
    TokBar,
    TokBarBar,
    
    // other delimiters
    TokColon,       //  Original has * for label, x for multiply, we use : for label * for multiply
    TokSemicolon,
    TokKetSquare,   // ]
    TokCall,        // !
    TokJmp,         // ^
    
    // entities
    TokName,
    TokIdentifier,
    TokIntegerLiteral,
    TokRealLiteral,
    
    // eof
    TokEof
};

// digraphs take presedence over unigraphs.
// The first representation in each reps vector is the preferred form.
struct HardwareRep
{
    Token token;
    std::vector<const char*> reps;
};

const HardwareRep hardwareRep[] =
{
        // george words
    {TokCos,     {"cos"}},
    {TokDup,     {"dup"}},
    {TokExp,     {"exp"}},
    {TokI,       {"I"}},
    {TokLog,     {"log"}},
    {TokMax,     {"max"}},
    {TokMod,     {"mod"}},
    {TokNeg,     {"neg"}},
    {TokPow,     {"pow"}},
    {TokPunch,   {"P"}},
    {TokReader,  {"R"}},
    {TokRev,     {"rev"}},
    {TokRem,     {"rem"}},
    {TokRep,     {"rep"}},
    {TokSin,     {"sin"}},
    {TokSqr,     {"sqr"}},
    {TokWait,    {"wait"}},

        // relational operators
    {TokGt,      {">"}},
    {TokEq,      {"="}},

        // arithmetic operators
    {TokPlus,    {"+"}},
    {TokMinus,   {"-"}},
    {TokTimes,   {"*"}},
    {TokDiv,     {"%"}},

        // Boolean operators
    {TokAnd,     {"&"}},
    {TokOr,      {"\\/"}},
    {TokNot,     {"~"}},

        // subscript operators
    {TokBar,     {"|"}},
    {TokBarBar,  {"||"}},
        
        // other delimiters
    {TokColon,   {":"}},
    {TokSemicolon,   {";"}},
    {TokKetSquare,   {"]"}},
    {TokCall,    {"!"}}, // down arrow
    {TokJmp,     {"^"}}, // up arrow
};

/**
 * Output token as text
 * @param[in] os output stream
 * param[in] t token
 */
std::ostream& operator<<(std::ostream& os, const Token t);

} // namespace george

#endif // _TOKENS_H_