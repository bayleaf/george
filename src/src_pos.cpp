#include "src_pos.h"
#include <sstream>

namespace george
{
SrcPos::SrcPos(int line, int col)
        : m_line(line), m_col(col)
{
}

int SrcPos::GetCol() const
{
    return m_col;
}

void SrcPos::SetCol(int col)
{
    m_col = col;
}

int SrcPos::GetLine() const
{
    return m_line;
}

void SrcPos::SetLine(int line)
{
    m_line = line;
}

std::string SrcPos::ToString() const
{
    std::ostringstream ss;
    ss << m_line << ":" << m_col;
    return ss.str();
}

std::ostream& operator<<(std::ostream& os, const SrcPos& p)
{
    os << p.ToString();
    return os;
}

} // namespace george
