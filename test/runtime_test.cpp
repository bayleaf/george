#include <gtest/gtest.h>
#include <parser.h>
#include <runtime.h>
#include <cstring>
#include <iostream>
#include <sstream>

class RuntimeFixture: public ::testing::Test
{
public:
    void RunMe(std::stringstream& src, std::istream& reader, std::ostream& punch);
};

void RuntimeFixture::RunMe(std::stringstream& src, std::istream& reader, std::ostream& punch)
{
    george::Lexer lex(src);
    george::Parser parser(lex);
    parser.Program();
    
    EXPECT_EQ(0, lex.GetErrors());
    george::Runtime runtime;
    runtime.Run(parser.GetProgram(), parser.GetLabelToIp(), reader, punch);
}

TEST_F(RuntimeFixture, FixtureTest)
{
    
}

TEST_F(RuntimeFixture, PunchTest)
{
    std::stringstream src;
    src << "42 (P) ; ]";
    
    std::stringstream reader;
    std::stringstream punch;
    RunMe(src, reader, punch);
    
    double result{0};
    punch >> result;
    
    EXPECT_EQ(42, result);
}  

TEST_F(RuntimeFixture, ArithmeticTest)
{
    std::stringstream src;
    src << "5 6 + 10 * 2 % (P) ; ]";
    
    std::stringstream reader;
    std::stringstream punch;
    RunMe(src, reader, punch);
    
    double result{0};
    punch >> result;
    
    EXPECT_EQ(55, result);
}
    
TEST_F(RuntimeFixture, LoadStoreTest)
{
    std::stringstream src;
    src << "1832 (a) ; a (P) ; ]";
    
    std::stringstream reader;
    std::stringstream punch;
    RunMe(src, reader, punch);
    
    double result{0};
    punch >> result;
    
    EXPECT_EQ(1832, result);
}

TEST_F(RuntimeFixture, SubroutineTest)
{
    std::stringstream src;
    src << "10! ]"
        << ":10 42 (P) ; ]";
    
    std::stringstream reader;
    std::stringstream punch;
    RunMe(src, reader, punch);
    
    double result{0};
    punch >> result;
    
    EXPECT_EQ(42, result);
}  

TEST_F(RuntimeFixture, MatrixTest)
{
    std::stringstream src;
    src << "1 3 1 3 R|| (a) 1 3 1 3 P|| (a)";
    
    std::stringstream reader;
    reader << "11 12 13 21 22 23 31 32 33";
    std::stringstream punch;
    RunMe(src, reader, punch);
    
    for (int i = 1; i <= 3; ++i)
    {
        for (int j = 1; j <= 3; ++j)
        {
            double v{0};
            punch >> v;
            EXPECT_EQ(i * 10 + j, v);
        }
    }
}

TEST_F(RuntimeFixture, RemTest)
{
    std::stringstream src;
    src << "10 3 rem (P) ;"
        << "10 3 neg rem (P) ;"
        << "10 neg 3 rem (P) ;"
        << "10 neg 3 neg rem (P) ;";
    
    std::stringstream reader;
    std::stringstream punch;
    RunMe(src, reader, punch);
    
    double v;
    punch >> v;
    EXPECT_EQ(1, v);
    
    punch >> v;
    EXPECT_EQ(1, v);
    
    punch >> v;
    EXPECT_EQ(-1, v);
    
    punch >> v;
    EXPECT_EQ(-1, v);
}

    
int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
