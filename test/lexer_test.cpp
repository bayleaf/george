#include <gtest/gtest.h>
#include <lexer.h>
#include <cstring>
#include <iostream>
#include <sstream>

class LexerFixture: public ::testing::Test
{
};

TEST_F(LexerFixture, FixtureTest)
{
    
}

TEST_F(LexerFixture, SimpleTest1)
{
    std::stringstream ss;
    ss << "1 n rep (j) j | a 2 pow |(t); ] (a comment (nested))";
    george::Lexer lex(ss);
    
    EXPECT_TRUE(lex.Have(george::TokIntegerLiteral));
    EXPECT_TRUE(lex.Have(george::TokIdentifier));
    EXPECT_TRUE(lex.Have(george::TokRep));
    
    EXPECT_TRUE(lex.Have(george::TokName));
    
    EXPECT_TRUE(lex.Have(george::TokIdentifier));
    
    EXPECT_TRUE(lex.Have(george::TokBar));
    EXPECT_TRUE(lex.Have(george::TokIdentifier));

    EXPECT_TRUE(lex.Have(george::TokIntegerLiteral));
    
    EXPECT_TRUE(lex.Have(george::TokPow));
    
    EXPECT_TRUE(lex.Have(george::TokBar));
    EXPECT_TRUE(lex.Have(george::TokName));
    
    EXPECT_TRUE(lex.Have(george::TokSemicolon));
    
    EXPECT_TRUE(lex.Have(george::TokKetSquare));

    EXPECT_TRUE(lex.Have(george::TokEof));
}

TEST_F(LexerFixture, SimpleTest2)
{
    std::stringstream ss;
    ss << "42 (P) ; ]";
    george::Lexer lex(ss);
    
    EXPECT_TRUE(lex.Have(george::TokIntegerLiteral));
    EXPECT_TRUE(lex.Have(george::TokPunch));
    EXPECT_TRUE(lex.Have(george::TokSemicolon));
    EXPECT_TRUE(lex.Have(george::TokKetSquare));
    EXPECT_TRUE(lex.Have(george::TokEof));
}    
    
TEST_F(LexerFixture, IntegerLiteralTest)
{
    std::stringstream ss;
    ss << "0 1 42 100000";
    george::Lexer lex(ss);
    
    int intValue{0};
    EXPECT_TRUE(lex.HaveIntegerLiteral(intValue));
    EXPECT_EQ(0, intValue);
    
    EXPECT_TRUE(lex.HaveIntegerLiteral(intValue));
    EXPECT_EQ(1, intValue);
    
    EXPECT_TRUE(lex.HaveIntegerLiteral(intValue));
    EXPECT_EQ(42, intValue);
    
    EXPECT_TRUE(lex.HaveIntegerLiteral(intValue));
    EXPECT_EQ(100000, intValue);
    
    EXPECT_TRUE(lex.Have(george::TokEof));
}

TEST_F(LexerFixture, RealLiteralTest)
{
    std::stringstream ss;
    ss << "0.0 .1 1e12 1e-13 42.8e+6 68.56e+4 34e12 1e7";
    george::Lexer lex(ss);
    
    double realValue{0.0};
    EXPECT_TRUE(lex.HaveRealLiteral(realValue));
    EXPECT_EQ(0.0, realValue);
    
    EXPECT_TRUE(lex.HaveRealLiteral(realValue));
    EXPECT_EQ(0.1, realValue);
    
    EXPECT_TRUE(lex.HaveRealLiteral(realValue));
    EXPECT_EQ(1e12, realValue);
    
    EXPECT_TRUE(lex.HaveRealLiteral(realValue));
    EXPECT_EQ(1e-13, realValue);
    
    EXPECT_TRUE(lex.HaveRealLiteral(realValue));
    EXPECT_EQ(42.8e6, realValue);
    
    EXPECT_TRUE(lex.HaveRealLiteral(realValue));
    EXPECT_EQ(68.56e+4, realValue);
    
    EXPECT_TRUE(lex.HaveRealLiteral(realValue));
    EXPECT_EQ(34e+12, realValue);
    
    EXPECT_TRUE(lex.HaveRealLiteral(realValue));
    EXPECT_EQ(1e+7, realValue);
    
    EXPECT_TRUE(lex.Have(george::TokEof));
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
